#! /usr/bin/python3

# Copyright (C) 2020 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import argparse
import json
import subprocess
import sys

NAGIOS_STATUS = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 'UNKNOWN': 3}


def get_options():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', metavar='PATH',
                        type=str,
                        help='path to check')
    parser.add_argument('--bind-mounts', dest='include_bindmounts', metavar='BOOL',
                        type=lambda x: (str(x).lower() in ['true', 'yes']),
                        default=False, const=True, nargs='?',
                        help='whether a bind mount should count as a mount point')
    parser.add_argument('--is-mount-point', dest='existence', metavar='BOOL',
                        type=lambda x: (str(x).lower() in ['true', 'yes']),
                        default=True, const=True, nargs='?',
                        help='whether to require the path to be a mount point (or not to be)')

    return parser.parse_args()


def process_mount_data(mounts):
    if not len(mounts):
        return {}

    mount_data = {mount['target']:
                  {'name': mount['target'],
                   'is_bind_mount': bool('[' in mount['source']),
                   } for mount in mounts
                  }

    child_data = process_mount_data(
        [child for mount in mounts
         for child in mount.get('children', [])
         if 'children' in mount]
    )

    return dict(mount_data, **child_data)


def get_mounts():
    args = [
            'findmnt',
            '--noheadings',
            '--json',
            '--output',
            'target,source'
            ]

    mount_data = json.loads(subprocess.check_output(args).decode('utf-8'))
    return process_mount_data(mount_data['filesystems'])


def main():
    args = get_options()

    mounts = get_mounts()

    is_mountpoint = args.path in mounts
    if not args.include_bindmounts:
        is_mountpoint = is_mountpoint and not mounts[args.path]['is_bind_mount']
    result = is_mountpoint if args.existence else not is_mountpoint

    if result:
        print('OK: {0} is{1} a mount point, as expected'.format(
              args.path, '' if args.existence else ' not'))
        sys.exit(NAGIOS_STATUS['OK'])
    else:
        print('CRITICAL: {0} is{1} a mount point'.format(
              args.path, ' not' if args.existence else ''))
        sys.exit(NAGIOS_STATUS['CRITICAL'])


if __name__ == '__main__':
    main()
