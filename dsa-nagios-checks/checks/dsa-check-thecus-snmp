#!/bin/bash
# check_thecus_snmp.pl for nagios
# altered version of check_snmp_synology by Nicolas Ordonez, Switzerland relkeased in 2016
#
# Version: 1
# 24-02-2020 A. Vlaanderen, Netherlands
#
# Thanks to:
# Nicolas Ordonez (original script writer)
#
#---------------------------------------------------
# this plugin checks the health of your Thecus NAS
# - System status
# - System info
# - Disks status
# - RAID status
# - System Temperature Warning and Critical
# - UPS information (needs implementation)
# - Storage percentage in use
#
# Tested and working for Firmwares >= 2.06.02.10
# on N16000pro and (N)12000(pro)
#---------------------------------------------------
# Based on NAS MIBS provided in the firmware of Thecus
#---------------------------------------------------

SNMPWALK=$(which snmpwalk)
SNMPGET=$(which snmpget)

SNMPVersion="3"
SNMPV2Community="public"
SNMPTimeout="5"
warningTemperature="70"
criticalTemperature="90"
warningStorage="90"
criticalStorage="95"
hostname=""
healthWarningStatus=0
healthCriticalStatus=0
healthString=""
verbose="no"
checkDSMUpdate="no"
ups="no" #UNCONFIGURED!

#OID declarations
OID_syno=".1.3.6.1.4.1.38243.1"
OID_model=".1.3.6.1.4.1.38243.1.1.2.0"
#OID_serialNumber="1.3.6.1.4.1.6574.1.5.2.0"
OID_OSVersion=".1.3.6.1.4.1.38243.1.1.3.0"
#OID_DSMUpgradeAvailable="1.3.6.1.4.1.6574.1.5.4.0"
OID_systemStatus=".1.3.6.1.4.1.38243.1.1.4.0"
OID_temperature=".1.3.6.1.4.1.38243.1.1.5.0"
OID_powerStatus="1.3.6.1.4.1.6574.1.3.0"
OID_systemFanStatus=".1.3.6.1.4.1.38243.1.1.6.0"
#OID_CPUFanStatus="1.3.6.1.4.1.6574.1.4.2.0"

#OID_disk=""
#OID_disk2=""
OID_diskID="1.3.6.1.4.1.38243.1.2.1.1.3"
OID_diskModel="1.3.6.1.4.1.38243.1.2.1.1.4"
OID_diskStatus="1.3.6.1.4.1.38243.1.2.1.1.6"
OID_diskSMARTHealthCheck="1.3.6.1.4.1.38243.1.2.1.1.7"
OID_diskSMARTReallocated_Sector_Ct="1.3.6.1.4.1.38243.1.2.1.1.8"
OID_diskTemp="1.3.6.1.4.1.38243.1.2.1.1.11"
OID_diskSMARTPendingSector="1.3.6.1.4.1.38243.1.2.1.1.12"

OID_RAID=""
OID_RAIDName=".1.3.6.1.4.1.38243.1.3.1.1.3"
OID_RAIDStatus=".1.3.6.1.4.1.38243.1.3.1.1.5"
OID_Storage=".1.3.6.1.4.1.38243.1.3.1.1.4"
OID_StorageSizeUsed=".1.3.6.1.4.1.38243.1.3.1.1.8"

#OID_StorageDesc="1.3.6.1.2.1.25.2.3.1.3"
#OID_StorageAllocationUnits="1.3.6.1.2.1.25.2.3.1.4"
#OID_StorageSize="1.3.6.1.2.1.25.2.3.1.5"
#BELOW NEEDS ADJUSTMENT IF UPS INFO IS WANTED
OID_UpsModel="1.3.6.1.4.1.6574.4.1.1.0"
OID_UpsSN="1.3.6.1.4.1.6574.4.1.3.0"
OID_UpsStatus="1.3.6.1.4.1.6574.4.2.1.0"
OID_UpsLoad="1.3.6.1.4.1.6574.4.2.12.1.0"
OID_UpsBatteryCharge="1.3.6.1.4.1.6574.4.3.1.1.0"
OID_UpsBatteryChargeWarning="1.3.6.1.4.1.6574.4.3.1.4.0"

usage()
{
        echo "usage: ./check_thecus_snmp [OPTIONS] -u [user] -p [pass] -h [hostname]"
        echo "options:"
        echo "            -u [snmp username]   	Username for SNMPv3"
        echo "            -p [snmp password]   	Password for SNMPv3"
        echo ""
        echo "            -2 [community name]	  	Use SNMPv2 (no need user/password) & define community name (ex: public)"
        echo ""
        echo "            -h [hostname or IP](:port)	Hostname or IP. You can also define a different port"
        echo ""
        echo "            -W [warning temp]		Warning temperature (for disks & synology) (default $warningTemperature)"
        echo "            -C [critical temp]		Critical temperature (for disks & synology) (default $criticalTemperature)"
        echo ""
        echo "            -w [warning %]		Warning storage usage percentage (default $warningStorage)"
        echo "            -c [critical %]		Critical storage usage percentage (default $criticalStorage)"
        echo ""
#####        echo "            -i   			Ignore DSM updates"
        echo "            -U   			Show informations about the connected UPS (only information, no control)"
        echo "            -v   			Verbose - print all informations about your Synology"
        echo ""
        echo ""
        echo "examples:	./check_thecus_snmp -u admin -p 1234 -h nas.intranet"	
        echo "	     	./check_thecus_snmp -u admin -p 1234 -h nas.intranet -v"	
        echo "		./check_thecus_snmp -2 public -h nas.intranet"	
        echo "		./check_thecus_snmp -2 public -h nas.intranet:10161"
        echo "		./check_thecus_snmp -2 public -h 192.168.0.186 -v"
        exit 3
}

if [ "$1" == "--help" ]; then
    usage; exit 0
fi

while getopts 2:W:C:w:c:u:p:h:iUv OPTNAME; do
        case "$OPTNAME" in
	u)	SNMPUser="$OPTARG";;
        p)	SNMPPassword="$OPTARG";;
        h)	hostname="$OPTARG";;
        v)	verbose="yes";;
	2)	SNMPVersion="2"
		SNMPV2Community="$OPTARG";;
	w)	warningStorage="$OPTARG";;
        c)      criticalStorage="$OPTARG";;
	W)	warningTemperature="$OPTARG";;
	C)	criticalTemperature="$OPTARG";;
	U)	ups="yes";;
        *)	usage;;
        esac
done

if [ "$warningTemperature" -gt "$criticalTemperature" ] ; then
    echo "Critical temperature must be higher than warning temperature"
    echo "Warning temperature: $warningTemperature"
    echo "Critical temperature: $criticalTemperature"
    echo ""
    echo "For more information:  ./${0##*/} --help"
    exit 1
fi

if [ "$warningStorage" -gt "$criticalStorage" ] ; then
    echo "The Critical storage usage percentage  must be higher than the warning storage usage percentage"
    echo "Warning: $warningStorage"
    echo "Critical: $criticalStorage"
    echo ""
    echo "For more information:  ./${0##*/} --help"
    exit 1
fi

if [ "$hostname" = "" ] || ([ "$SNMPVersion" = "3" ] && [ "$SNMPUser" = "" ]) || ([ "$SNMPVersion" = "3" ] && [ "$SNMPPassword" = "" ]) ; then
    usage
else
    if [ "$SNMPVersion" = "2" ] ; then
	SNMPArgs=" -OQne -v 2c -c $SNMPV2Community -t $SNMPTimeout"
    else
	SNMPArgs=" -OQne -v 3 -u $SNMPUser -A $SNMPPassword -l authNoPriv -a MD5 -t $SNMPTimeout"
	if [ ${#SNMPPassword} -lt "8" ] ; then
	    echo "snmpwalk:  (The supplied password is too short.)"
	    exit 1
	fi
    fi
    syno=`$SNMPWALK $SNMPArgs $hostname $OID_syno 2> /dev/null`
    if [ "$?" != "0" ] ; then
        echo "CRITICAL - Problem with SNMP request, check user/password/host"
        exit 2
   fi


    nbDisk=$(echo "$syno" | grep $OID_diskID | wc -l)
#    nbRAID=$(echo "$syno" | grep $OID_RAIDNameW | wc -l)


#    for i in `seq 1 $nbDisk`;
#   do
#	if [ $i -lt 25 ] ; then
#	    OID_disk="$OID_disk $OID_diskID.$i $OID_diskModel.$i $OID_diskStatus.$i $OID_diskTemp.$i "
#	else
#	    OID_disk2="$OID_disk2 $OID_diskID.$(($i-1)) $OID_diskModel.$(($i-1)) $OID_diskStatus.$(($i-1)) $OID_diskTemp.$(($i-1)) "
#	fi
#    done

###    for i in `seq 1 $nbRAID`;
###    do
###      OID_RAID="$OID_RAIDName.$(($i-1)) $OID_RAIDStatus.$(($i-1))"

      #OID_RAID="$OID_RAIDName.$(($i-1)) $OID_RAIDStatus.$(($i-1))"
###    done

#TEST
#echo "OIDRAID: $OID_RAID - $nbRAID - $RAIDName"
#


#    if [ "$ups" = "yes" ] ; then
#	syno=`$SNMPGET $SNMPArgs $hostname $OID_model $OID_OSVersion $OID_systemStatus $OID_temperature $OID_systemFanStatus $OID_RAID $OID_StorageSizeUsed $OID_UpsModel $OID_UpsSN $OID_UpsStatus $OID_UpsLoad $OID_UpsBatteryCharge $OID_UpsBatteryChargeWarning 2> /dev/null`
#    else
#	syno=`$SNMPGET $SNMPArgs $hostname $OID_model $OID_OSVersion $OID_systemStatus $OID_temperature $OID_systemFanStatus $OID_RAID $OID_RAIDName $OID_RAIDStatus $OID_StorageSizeUsed $OID_disk 2> /dev/null`
#    fi

#    if [ "$OID_disk2" != "" ]; then
#	syno2=`$SNMPGET $SNMPArgs $hostname $OID_disk2 2> /dev/null`
#	syno=$(echo "$syno";)
#    fi
    nbRAID=$(echo "$syno" | grep $OID_RAIDName | wc -l)

    model=$(echo "$syno" | grep $OID_model | cut -d "=" -f2 | sed 's/[^a-zA-Z0-9.]//g')
#    serialNumber=$(echo "$syno" | grep $OID_serialNumber | cut -d "=" -f2)
    OSVersion=$(echo "$syno" | grep $OID_OSVersion | cut -d "=" -f2 | sed 's/[^a-zA-Z0-9.]//g')

    healthString="Thecus $model"

#####    DSMUpgradeAvailable=$(echo "$syno" | grep $OID_DSMUpgradeAvailable | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
#    case $DSMUpgradeAvailable in
#	"1")	DSMUpgradeAvailable="Available";	if [ "$checkDSMUpdate" = "yes" ]; then healthWarningStatus=1;		healthString="$healthString, DSM update available"; fi ;;
#	"2")	DSMUpgradeAvailable="Unavailable";;
#	"3")	DSMUpgradeAvailable="Connecting";;					
#	"4")	DSMUpgradeAvailable="Disconnected";	if [ "$checkDSMUpdate" = "yes" ]; then healthWarningStatus=1;		healthString="$healthString, DSM Update Disconnected"; fi ;;
#	"5")	DSMUpgradeAvailable="Others";		if [ "$checkDSMUpdate" = "yes" ]; then healthWarningStatus=1;		healthString="$healthString, Check DSM Update"; fi ;;
#   esac

    systemStatus=$(echo "$syno" | grep $OID_systemStatus | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
    temperature=$(echo "$syno" | grep $OID_temperature | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
#   powerStatus=$(echo "$syno" | grep $OID_powerStatus | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
    systemFanStatus=$(echo "$syno" | grep $OID_systemFanStatus | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')


   # CPUFanStatus=$(echo "$syno" | grep $OID_CPUFanStatus | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')

    #Check system status
    if [ "$systemStatus" = "0" ] ; then
	systemStatus="Normal"
    else
 	systemStatus="Failed"
        healthCriticalStatus=1
        healthString="$healthString, System status: $systemStatus "
    fi

    #Check system temperature
    if [ "$temperature" -gt "$warningTemperature" ] ; then
    	if [ "$temperature" -gt "$criticalTemperature" ] ; then
        	temperature="$temperature (CRITICAL)"
	        healthCriticalStatus=1
	        healthString="$healthString, temperature: $temperature "
	else
	        temperature="$temperature (WARNING)"
        	healthWarningStatus=1
	        healthString="$healthString, temperature: $temperature "
	fi
    else
	temperature="$temperature°C"
    fi


    #Check power status
#    if [ "$powerStatus" = "1" ] ; then
#        powerStatus="Normal"
#    else
#       	powerStatus="Failed";
#        healthCriticalStatus=1
#        healthString="$healthString, Power status: $powerStatus "
#    fi


    #Check system fan status
    if [ "$systemFanStatus" = "0" ] ; then
        systemFanStatus="Normal"
    else
        systemFanStatus="Failed";		
        healthCriticalStatus=1
        healthString="$healthString, System fan status: $systemFanStatus "
    fi



#    if [ "$CPUFanStatus" = "1" ] ; then
#	CPUFanStatus="Normal"
#    else
#        CPUFanStatus="Failed";		
#        healthCriticalStatus=1
#        healthString="$healthString, CPU fan status: $CPUFanStatus "
#    fi


    #Check all disk status
    for i in `seq 1 $nbDisk`;
    do
        diskID[$i]=$(echo "$syno" | grep "$OID_diskID.$i " | cut -d "=" -f2)
        diskModel[$i]=$(echo "$syno" | grep "$OID_diskModel.$i " | cut -d "=" -f2 )
        diskStatus[$i]=$(echo "$syno" | grep "$OID_diskStatus.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
        diskTemp[$i]=$(echo "$syno" | grep "$OID_diskTemp.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
        diskSMARTHealthCheck[$i]=$(echo "$syno" | grep "$OID_diskSMARTHealthCheck.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
        diskSMARTReallocated_Sector_Ct[$i]=$(echo "$syno" | grep "$OID_diskSMARTReallocated_Sector_Ct.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
        diskSMARTPendingSector[$i]=$(echo "$syno" | grep "$OID_diskSMARTPendingSector.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')

	case ${diskStatus[$i]} in
		"0")	diskStatus[$i]="Clean";		;;
		"1")	diskStatus[$i]="Initialized";		;;
		"2")	diskStatus[$i]="Other";	;;
		"3")	diskStatus[$i]="Error";	healthCriticalStatus=1; healthString="$healthString, problem with ${diskID[$i]} (model:${diskModel[$i]}) status:${diskStatus[$i]} temperature:${diskTemp[$i]}";;
	esac
        if [ ${diskSMARTHealthCheck[$i]} -ne 0 ]; then
                healthCriticalStatus=1
                healthString="$healthString, ${diskID[$i]} SMART test failed"
        fi
        if [ ${diskSMARTReallocated_Sector_Ct[$i]} -gt 0 ]; then
                healthCriticalStatus=1
                healthString="$healthString, ${diskID[$i]} reallocated sector count ${diskSMARTReallocated_Sector_Ct[$i]}"
        fi
        if [ ${diskSMARTPendingSector[$i]} -gt 0 ]; then
                healthCriticalStatus=1
                healthString="$healthString, ${diskID[$i]} pending sector count ${diskSMARTPendingSector[$i]}"
        fi

	if [ "${diskTemp[$i]}" -gt "$warningTemperature" ] ; then
	    if [ "${diskTemp[$i]}" -gt "$criticalTemperature" ] ; then
		diskTemp[$i]="${diskTemp[$i]} (CRITICAL)"
		healthCriticalStatus=1;
		healthString="$healthString, ${diskID[$i]} temperature: ${diskTemp[$i]}"
	    else
		diskTemp[$i]="${diskTemp[$i]} (WARNING)"
		healthWarningStatus=1;
		healthString="$healthString, ${diskID[$i]} temperature: ${diskTemp[$i]}"
	    fi
	fi

     done

#    syno_diskspace=`$SNMPWALK $SNMPArgs $hostname $OID_Storage 2> /dev/null`

#Check all RAID volume status
#seqRaid= `seq 1 $nbRAID`
#echo "nbRAID:  $nbRAID";



    for i in `seq 1 $nbRAID`;
    do
	RAIDName[$i]=$(echo "$syno" | grep "$OID_RAIDName.$i[^\.0-9]" | cut -d "=" -f2)
	RAIDStatus[$i]=$(echo "$syno" | grep "$OID_RAIDStatus.$i[^\.0-9]" | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
	storageName[$i]=$(echo "${RAIDName[$i]}" | sed -e 's/[[:blank:]]//g' | sed -e 's/\"//g' | sed 's/.*/\L&/')
        storageUsedStatus[$i]=$(echo "$syno" | grep "$OID_StorageSizeUsed.$i " | cut -d "=" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')

case ${RAIDStatus[$i]} in
		"0")	RAIDStatus[$i]="Healthy";		;;
		"1")	RAIDStatus[$i]="Degraded";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"2")	RAIDStatus[$i]="Damaged";		healthCriticalStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"3")	RAIDStatus[$i]="Creating";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"4")	RAIDStatus[$i]="Formatting";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"5")	RAIDStatus[$i]="Building";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"6")	RAIDStatus[$i]="Waiting";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"7")	RAIDStatus[$i]="Recovering";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"8")	RAIDStatus[$i]="Migrating";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"9")	RAIDStatus[$i]="Unknown";		healthCriticalStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"10")	RAIDStatus[$i]="Decrypt_Fail";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;
		"11")	RAIDStatus[$i]="Encrypt_Fail";		healthWarningStatus=1;		healthString="$healthString, RAID status (${RAIDName[$i]}): ${RAIDStatus[$i]} ";;

esac
#Check Storage size status

        if [ "${storageUsedStatus[$i]}" -gt "$criticalStorage" ] ; then
                StorageStatus[$i]="Critical";
                healthCriticalStatus=1
                healthString="$healthString, ${storageName[$i]} ${StorageStatus[$i]} ${storageUsedStatus[$i]}% used"
        elif [ "${storageUsedStatus[$i]}" -gt "$warningStorage" ] ; then
                StorageStatus[$i]="Warning";
                healthWarningStatus=1
                healthString="$healthString, ${storageName[$i]} ${StorageStatus[$i]} ${storageUsedStatus[$i]}% used"
        elif [ "${storageUsedStatus[$i]}" -lt "$warningStorage" ] ; then
                StorageStatus[$i]="Normal";
                healthString="$healthString, ${storageName[$i]} ${StorageStatus[$i]} ${storageUsedStatus[$i]}% used"
  fi

done
if [ "$verbose" = "yes" ] ; then
	echo "Thecus model:		$model" ;
#	echo "Thecus s/n:		$serialNumber" ;
	echo "OS Version:		$OSVersion" ;
	echo "System Status:		 $systemStatus" ;
	echo "Temperature:		 $temperature" ;
#	echo "Power Status:		 $powerStatus" ;
	echo "System Fan Status:	 $systemFanStatus" ;
	echo "Raid Type:		 $RAIDName" ;

#	echo "CPU Fan Status:		 $CPUFanStatus" ;
	echo "Number of disks:         $nbDisk" ;
	for i in `seq 1 $nbDisk`;
	do
		echo " ${diskID[$i]} (model:${diskModel[$i]}) status:${diskStatus[$i]} temperature:${diskTemp[$i]}" ;
	done
#	echo "Number of RAID volume:   $nbRAID" ;
 	for i in `seq 1 $nbRAID`;
    	do
	echo " ${RAIDName[$i]} raid status			: ${RAIDStatus[$i]}" ;
	echo " ${RAIDName[$i]} storage capacity status 	: ${StorageStatus[$i]}" ;
	echo " ${RAIDName[$i]} percentage used 		: ${storageUsedStatus[$i]}%" ;
	done

	# Display UPS information
    	if [ "$ups" = "yes" ] ; then
            upsModel=$(echo "$syno" | grep $OID_UpsModel | cut -d "=" -f2)
            upsSN=$(echo "$syno" | grep $OID_UpsSN | cut -d "=" -f2)
            upsStatus=$(echo "$syno" | grep $OID_UpsStatus | cut -d "=" -f2)
            upsLoad=$(echo "$syno" | grep $OID_UpsLoad | cut -d "=" -f2)
            upsBatteryCharge=$(echo "$syno" | grep $OID_UpsBatteryCharge | cut -d "=" -f2)
            upsBatteryChargeWarning=$(echo "$syno" | grep $OID_UpsBatteryChargeWarning | cut -d "=" -f2)

	    echo "UPS:"
	    echo "  Model:		$upsModel"
	    echo "  s/n:			$upsSN"
	    echo "  Status:		$upsStatus"
	    echo "  Load:			$upsLoad"
	    echo "  Battery charge:	$upsBatteryCharge"
	    echo "  Battery charge warning:$upsBatteryChargeWarning"
	fi

	echo "";
    fi

    if [ "$healthCriticalStatus" = "1" ] ; then
	echo "CRITICAL - $healthString"
	exit 2
    fi
    if [ "$healthWarningStatus" = "1" ] ; then
	echo "WARNING - $healthString"
	exit 1
    fi
    if [ "$healthCriticalStatus" = "0" ] && [ "$healthWarningStatus" = "0" ] ; then
	echo "OK - $healthString is in good health"
	exit 0
    fi
fi


# AVAILABLE OID's sorted:
# OID INFO THECUS
#
# ------------------
# RAIDstatus:
# ------------------
# 0 = Healthy
# 1 = Degraded
# 2 = Damaged
# 3 = Creating
# 4 = Formatting
# 5 = Building
# 6 = Waiting
# 7 = Recovering
# 8 = Migrating
# 9 = Unknown
# 10=Decrypt_Fail
# 11=Encrypt_Fail
#
# ------------------
# UPS Status codes:
# ------------------
# 0 = Not Plugin
# 1 = Powered
# 2 = Full
# 3 = Charging
# 4 = Unknown
# 5 = No Mini-UPS
# ------------------
#
#
# ------------------
# OID LIST
# ------------------
# Manufacturer							.1.3.6.1.4.1.38243.1.1.1.0 = "THECUS"
# SystemModel							.1.3.6.1.4.1.38243.1.1.2.0 = "N16000PRO"
# Firmware 							.1.3.6.1.4.1.38243.1.1.3.0 = "2.06.02.10"
# system status 						.1.3.6.1.4.1.38243.1.1.4.0 = 0
# temperatuur 							.1.3.6.1.4.1.38243.1.1.5.0 = 67
# fanstatus    							.1.3.6.1.4.1.38243.1.1.6.0 = 0
# Mini-UPS Status						.1.3.6.1.4.1.38243.1.1.7.0 = 5
# HDD DISCOVERY							.1.3.6.1.4.1.38243.1.2.1.1.2.0 = 1
# HDD	 TrayID							.1.3.6.1.4.1.38243.1.2.1.1.2.1 = 1
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.2 = 2
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.3 = 3
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.4 = 4
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.5 = 5
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.6 = 6
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.7 = 7
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.8 = 8
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.9 = 9
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.10 = 10
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.11 = 11
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.12 = 12
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.13 = 13
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.14 = 14
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.15 = 15
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.2.16 = 16
# HDD	Disk ID							.1.3.6.1.4.1.38243.1.2.1.1.3.1 = "sda"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.2 = "sdb"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.3 = "sdc"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.4 = "sdd"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.5 = "sde"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.6 = "sdf"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.7 = "sdg"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.8 = "sdh"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.9 = "sdi"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.10 = "sdj"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.11 = "sdk"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.12 = "sdl"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.13 = "sdm"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.14 = "sdn"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.15 = "sdo"
# HDD								.1.3.6.1.4.1.38243.1.2.1.1.3.16 = "sdp"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.1 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.2 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.3 = "ST4000NM0035-1V4"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.4 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.5 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.6 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.7 = "ST4000NM0035-1V4"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.8 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.9 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.10 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.11 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.12 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.13 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.14 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.15 = "HUS724040ALS640"
# HDD	Model							.1.3.6.1.4.1.38243.1.2.1.1.4.16 = "HUS724040ALS640"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.1 = "3907018584 KB"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.2 = "3907018584 KB"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.3 = "3907018584 KB"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.4 = "3907018584 KB"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.5 = "3907018584 KB"
# HDD	Size							.1.3.6.1.4.1.38243.1.2.1.1.5.6 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.7 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.8 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.9 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.10 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.11 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.12 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.13 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.14 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.15 = "3907018584 KB"
# HDD	Size							..1.3.6.1.4.1.38243.1.2.1.1.5.16 = "3907018584 KB"
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.1 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.2 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.3 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.4 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.5 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.6 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.7 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.8 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.9 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.10 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.11 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.12 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.13 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.14 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.15 = 1
# HDD Status Initialized					.1.3.6.1.4.1.38243.1.2.1.1.6.16 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.1 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.2 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.3 = 0
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.4 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.5 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.6 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.7 = 0
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.8 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.9 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.10 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.11 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.12 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.13 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.14 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.15 = 1
# HDD SMart Status (inacurate)					.1.3.6.1.4.1.38243.1.2.1.1.7.16 = 1
# HDD Reallocate sector >0 = bad				.1.3.6.1.4.1.38243.1.2.1.1.8.1 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.2 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.3 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.4 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.5 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.6 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.7 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.8 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.9 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.10 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.11 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.12 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.13 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.14 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.15 = 0
# HDD Reallocate sector						.1.3.6.1.4.1.38243.1.2.1.1.8.16 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.1 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.2 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.3 = 7919
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.4 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.5 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.6 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.7 = 9301
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.8 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.9 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.10 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.11 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.12 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.13 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.14 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.15 = 0
# HDD PowerOnHours						.1.3.6.1.4.1.38243.1.2.1.1.9.16 = 0
# HDD end-to-end error >0 = bad					.1.3.6.1.4.1.38243.1.2.1.1.10.1 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.2 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.3 = 0
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.4 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.5 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.6 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.7 = 0
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.8 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.9 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.10 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.11 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.12 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.13 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.14 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.15 = -1
# HDD end-to-end error						.1.3.6.1.4.1.38243.1.2.1.1.10.16 = -1
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.1 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.2 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.3 = 36
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.4 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.5 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.6 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.7 = 36
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.8 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.9 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.10 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.11 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.12 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.13 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.14 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.15 = 0
# HDD TEMP							.1.3.6.1.4.1.38243.1.2.1.1.11.16 = 0
# HDD Current__Pending_Sector >0 = error			.1.3.6.1.4.1.38243.1.2.1.1.12.1 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.2 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.3 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.4 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.5 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.6 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.7 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.8 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.9 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.10 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.11 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.12 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.13 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.14 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.15 = 0
# HDD Current__Pending_Sector					.1.3.6.1.4.1.38243.1.2.1.1.12.16 = 0
# RAID number discovery						.1.3.6.1.4.1.38243.1.3.1.1.2.0 = 0
# RAID number							.1.3.6.1.4.1.38243.1.3.1.1.2.1 = 0
# raidtype 							.1.3.6.1.4.1.38243.1.3.1.1.3.1 = "RAID50"
# raidid (description)						.1.3.6.1.4.1.38243.1.3.1.1.4.1 = "raid0"
# raidstatus (see list above)					.1.3.6.1.4.1.38243.1.3.1.1.5.1 = 0
# raidlun numbers						.1.3.6.1.4.1.38243.1.3.1.1.6.1 = "md30 md31"
# raidsize (in KB)						.1.3.6.1.4.1.38243.1.3.1.1.7.1 = "54661184896 KB"
# raidused perc							.1.3.6.1.4.1.38243.1.3.1.1.8.1 = 75
#
#
